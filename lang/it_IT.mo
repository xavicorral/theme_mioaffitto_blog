��    I      d  a   �      0  ,   1     ^  7   ~  4   �     �     �                    ,  
   1     <     E     _     l     y     �     �  C   �     �     �     �            D        a     f     m     u     �  >   �  	   �     �     �     	     	     	     	  ?   $	  *   d	  B   �	     �	     �	  "   
     &
     .
  	   6
  	   @
     J
     e
     
     �
  
   �
     �
  %   �
  	   �
     �
     �
  %   �
  C   !     e     n  �   �  *   #     N     k  8   �     �     �     �  �   �  !   �    �  #   �  '   �  7   %  5   ]  	   �     �     �     �     �     �  	   �  	   �     �          !     .     ;     L  Q   \     �     �     �     �     �  M   �     +     4  	   :     D     `  %   e     �     �     �     �  	   �     �     �  2   �  '     #   G     k     �     �     �  	   �  
   �     �     �     �            	   0     :     R     r     �     �  2   �  B   �            k   <      �     �     �  ,   �     (     7     J  �   R  +   6     7   9           8   (       <         !          ,   >   #            $   &   ?   =                 D      4   *       F   :          2       6   E      '          G   A   ;          /   1   -          I      5           3      
           B                        @              0   C          +      	   H   .   )             "   %                         page template has been successfully created  page template has been updated <a href="http://www.gravatar.com/">Gravatar</a> E-mail: <span class="meta-nav">&laquo;</span> Older Comments About the Author Ad Code: All Blog Posts Archive Author archives Bio: Categories Category Choose Your Page Template Comments (%) Comments (0) Comments (1) Comments are closed. Create Template E-mail has not been setup properly. Please add your contact e-mail! Example Follow From:  Group Hide the Content If you want to submit this form, do not enter anything in this field Left Medium Message Message via the contact form Name Need help? Use the Help tab in the upper right of your screen. Next Post Nothing found here! Number of comments Number: Page %s Pages Pages: Perhaps You will find something interesting form these lists... Photos on <span>flick<span>r</span></span> Please enter your email address (and please make sure it\'s valid) Please enter your message Please enter your name Please select your template first! Popular Preview Read Less Read More Read More Text (optional): Read More URL (optional): Read article Related Posts Reply-To:  Search Results Send a copy of this email to yourself Show More Show the Content Size: Sorry, no posts matched your criteria Sorry, the operation was unsuccessful for the following reason(s):  Sorting: Subscribe to our RSS feed The \"[template]\" shortcode already exists, possibly added by the theme or other plugins. Please consult with the theme author to consult with this issue The template has been successfully deleted Themnific - Blog Author Info Themnific - Comments There were one or more errors while submitting the form. This template is empty Title (optional): Title: To create a custom page template, give it a name above and click Create Template. Then choose blocks like text, widgets or tabs &amp; toggles from the left column to add to this template Your email was successfully sent. Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Project-Id-Version: it_IT
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
X-Generator: Poedit 1.7.1
Plural-Forms: nplurals=2; plural=(n != 1);
Language: it_IT
 Pagina template creata con successo Pagina template aggiornata con successo <a href="http://www.gravatar.com/">Gravatar</a> E-mail: <span class="meta-nav">&laquo;</span> Vecchi commenti L'autore: Ad Code: Tutti i post del blog Archivio Archivio dell'autore Bio: Categorie Categoria Scegli il template della pagina Commenti  (%) Commenti (0) Commenti (1) Commenti chiusi- Creare Template La email non è stata configurata correttamente. Aggiungi il tuo indirizzo email! Esempio Segui Da: Gruppo Nascondi il contenuto Se si desidera inviare il presente modulo, non inserire nulla in questo campo Sinistra Medio Messaggio Contattaci con questo form: Nome Bisogno di aiuto? Usa il tasto Aiuto  Prossimo post Non abbiamo trovato niente qui! Numero di commenti Numero: Pagina %s Pagine Pagine Troverai sicuramento qualcosa di interessante qui: Foto su<span>flick<span>r</span></span> Inserisci un indirizzo email valido Scrivi qui il tuo messaggio Il tuo nome Scegli il template prima! Popolare Anteprima Leggi meno Leggi di piú Leggi piú testo (opzionale) Guarda la URL (opzionale): Leggi il post Post relazionati Rispondi: Risultati della ricerca Inviami una copia del messaggio Mostra di piú Mostra il contenuto Dimensioni: Spiacente, nessun post corrisponde ai tuoi criteri Spiacenti, l'operazione non è riuscita per il seguente motivo(i): Ordine: Iscriviti al nostro feed RSS shortcode giá esistente, forse aggiunto dal tema o altri plugin. Si prega di consultare l'autore del tema  Template cancellato con successo Themnific - Info sull'autore Themnific - Commenti Uno o piú errori durante l'invio del modulo Template pieno Title (opzionale): Titolo: Per creare un modello di pagina personalizzata, dare un nome al di sopra e fare clic su Crea modello. Quindi scegliere blocchi come testo, widget o schede & amp; alterna dalla colonna di sinistra per aggiungere a questo modello La tua email è stata inviata con successo! 