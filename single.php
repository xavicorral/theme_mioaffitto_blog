<?php get_header();  

tmnf_count_views(get_the_ID());

$sidebar_opt = get_post_meta($post->ID, 'tmnf_sidebar', true);?>

<div id="core" class="<?php echo $sidebar_opt ?>">
    
    <div <?php post_class(); ?>  itemscope itemprop="blogPost" itemtype="http://schema.org/Article"> 
    
        
        <div class="clearfix"></div>
    
        <div id="content"><div class="eightcol left-side">
        	        <h1 class="post entry-title" itemprop="headline"><?php the_title(); ?></h1>

        
                <?php get_template_part('single-content' ); ?>
                
        </div><!-- #homecontent -->
        
        <?php if($sidebar_opt == 'None'){ } else { get_sidebar();} ?>
    
    </div>
    </div>

</div><!-- #core -->
    
<?php get_footer(); ?>