</div><div class="clerfix"></div>

    	<footer id="footer-ena">

            <div class="footer-social-links">
                <div class="container">
                    <ul class="list-unstyled list-inline">
                    <!--
                      <li class="first">
                            <a href="http://blog.mioaffitto.it/" rel="nofollow">
                                <i class="fa fa-rss left"></i>
                                <p>Blog</p>
                            </a>
                        </li>
                         -->
                        <li>
                            <a href="http://www.facebook.com/Mioaffitto" title="http://www.facebook.com/Mioaffitto" rel="nofollow">
                                <i class="fa fa-facebook-square left"></i>
                                <p>Facebook</p>
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/102293637834095377414?prsrc=3" rel="nofollow">
                                <i class="fa fa-google-plus left"></i>
                                <p>Google+</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://twitter.com/mioaffitto" rel="nofollow">
                                <i class="fa fa-twitter left"></i>
                                <p>Twitter</p>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.pinterest.com/mioaffitto/" rel="nofollow">
                                <i class="fa fa-pinterest left"></i>
                                <p>Pinterest</p>
                            </a>
                        </li>
                        <!--
                        <li>
                            <a href="http://instagram.com/mioaffitto.it" rel="nofollow">
                                <i class="fa fa-instagram left"></i>
                                <p>Instagram</p>
                            </a>
                        </li>
                        -->
                    </ul>
                </div>
            </div>


            <!--
            <div class="footer-intra-links">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-4 info-mercado-alquiler">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i>Información sobre el <b>Mercado del Alquiler</b></div>
                            <ul class="list-unstyled">
                                <li><a href="http://www.beta.enalquiler.com/precios/precio-alquiler-vivienda-espana_31-0-0-0.html" rel="nofollow">Evolución del precio del alquiler</a></li>
                                <li><a href="http://www.beta.enalquiler.com/ventajas-de-alquilar-para-el-propietario.html" rel="nofollow">Ventajas de alquilar: para el propietario</a></li>
                                <li><a href="http://www.beta.enalquiler.com/ventajas-de-alquilar-para-el-inquilino.html" rel="nofollow">Ventajas de alquilar: para el inquilino</a></li>
                            </ul>
                        </div>

                        <div class="col-sm-4 secciones-interes">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i><b>Enalquiler</b> en la red</div>
                            <ul class="list-unstyled">
                                <li><a href="http://www.beta.enalquiler.com/comunidad-alquiler/preguntar.html">Consulta tus dudas</a> sobre alquiler de pisos</li>
                                <li>Organiza tu <a href="http://www.beta.enalquiler.com/traslado-piso.html" rel="nofollow">Traslado de Piso</a></li>
                                <li><a href="http://www.beta.enalquiler.com/index.php/cod.go_page/page.frm_recomienda/" rel="nofollow">¡Recomienda Enalquiler a un amigo!</a></li>
                            </ul>
                        </div>

                        <div class="col-sm-4 sobre-enalquiler">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i>Sobre <b>Enalquiler</b></div>
                            <ul class="list-unstyled">
                                <li><a href="http://www.beta.enalquiler.com/que-es-enalquiler-com.html" rel="nofollow">¿Qué es enalquiler.com?</a></li>
                                <li><a href="http://www.beta.enalquiler.com/index.php/cod.faq_ayuda/" rel="nofollow">Preguntas frecuentes - Ayuda</a></li>
                                <li><a href="http://www.beta.enalquiler.com/publicidad.html" rel="nofollow">Publicidad</a></li>
                                <li><a href="http://www.beta.enalquiler.com/aviso-legal.html" rel="nofollow">Aviso Legal</a></li>
                                <li><a href="http://www.beta.enalquiler.com/sitemap/">Sitemap</a></li>
                                <li><a href="http://www.beta.enalquiler.com/busquedas/0/1" rel="nofollow"Últimas búsquedas</a></li>
                                <li><a href="http://www.beta.enalquiler.com/anunciar-piso-gratis" rel="nofollow"<b>Anuncia tu piso gratis</b></a></li>
                                <li><a href="http://www.beta.enalquiler.com/servicios-anunciantes.html" rel="nofollow">Servicios para anunciantes profesionales</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            -->
            
        <div class="footer-intra-links">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-4 info-mercado-alquiler">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i>Informazione sul <b>Mercato degli Affitti</b></div>
                            <ul class="list-unstyled">
                                <li><a href="http://www.mioaffitto.it/prezzi/prezzo-affitto-italia_31-0-0-0.html" rel="nofollow">Evoluzione del prezzo d' affitto</a></li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/vantaggi-dell-affitto-per-il-proprietario.html" rel="nofollow">Vantaggi dell' affitto: per il proprietario</a></li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/vantaggi-dell-affitto-per-l-inquilino.html" rel="nofollow">Vantaggi dell' affitto: per l' inquilino</a></li>
                            </ul>
                        </div>

                        <div class="col-sm-4 secciones-interes">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i><b>Mioaffitto</b> in rete</div>
                            <ul class="list-unstyled">
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/community_affitto/domande-risposte" rel="nofollow">Consulta i tuoi dubbi</a> sull' affitto degli appartamenti</li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/consigliaci-a-un-amico" rel="nofollow">Raccomanda Mioaffitto a un amico!</a></li>
                            </ul>
                        </div>

                        <div class="col-sm-4 sobre-enalquiler">
                            <div class="generic-light-title-sm"><i class="fa fa-angle-right"></i>Su <b>Mioaffitto</b></div>
                            <ul class="list-unstyled">
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/cosa-e-mio-affitto-it.html" rel="nofollow">Cos'&egrave Mioaffitto.it?</a></li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/faq-aiuto.html" rel="nofollow">Domande frequenti - Aiuto </a></li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/pubblicita.html" rel="nofollow">Pubblicit&agrave</a></li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/avviso-legale.html" rel="nofollow">Avviso Legale e Politica di Cookies</a></li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/annunciare-immobile-gratis" rel="nofollow"><b>Annuncia il tuo appartamento gratis</b></a></li>
                                <li><a class="linkstyle" href="http://www.mioaffitto.it/servizi-profesionisti.html" rel="nofollow">Servizi per annuncianti professionisti</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            


            <div class="grupo-intercom">
                <div class="container">
                    <img src="http://www.beta.enalquiler.com/front-end/images/grupo-intercom.png" alt="Grupo Intercom">
                </div>
            </div>

        </footer> 
        
	</div><!--  end .cotainer  --> 
   

<div class="scrollTo_top" style="display: block">

    <a title="Scroll to top" href="#">
    
    	<i class="fa fa-arrow-circle-o-up"></i>
        
    </a>
    
</div>
<?php themnific_foot(); ?>
<?php wp_footer();?>

</body>
</html>