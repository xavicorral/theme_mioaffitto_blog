        <div id="sidebar"  class="fourcol right-side">
        
        	<?php if(is_single())  {
        
           	get_template_part('single-sidebar' ); 
            
            } ?>
            
            <div class="widgetable">
    
            	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar") ) : ?>
            	<?php endif; ?>
            
            </div>
               
        </div><!-- #sidebar -->