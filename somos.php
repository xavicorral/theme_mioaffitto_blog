<?php
/*
Template Name: Somos
*/
?>

<?php get_header(); ?>

<div id="core">   
            
	<div id="content" class="eightcol"> 
        
            <h1 class="leading">Quiénes somos</h1>
            <p>
               Desde 2005 somos el único portal inmobiliario especialista en viviendas de alquiler en España. 
               Espacio y punto de encuentro on-line entre propietarios que desean alquilar sus viviendas y todas aquellas 
               personas que están buscando en estos momentos su nuevo hogar. Creemos en la cultura del alquiler, 
               y en la actualidad, somos referencia imprescindible en nuestro sector. 
               <strong>La vida cambia, las circunstancias cambian, nuestras necesidades, sueños y deseos también. </strong>

            </p>
            <h2 class="leading"></h2>
    
            <div class="clearfix"></div>
  

	
      <div class="postauthor postauthor_alt">
              <?php
              //display selected users
              $userids_to_display = array(83,2,37,107,92,97,105,106,103,104,99,95,100,96,91); // wordpress user IDs to include
             $blogusers = get_users_of_blog();
             $photoauthor = get_the_author_meta('photo');
            if ($blogusers) {
              foreach ($blogusers as $bloguser) {
                if ( in_array($bloguser->user_id, $userids_to_display) ) {
                  $user = get_userdata($bloguser->user_id);
                  echo '<p class="authordesc">';
                  echo '<img class="postauthor" src="'.$photoauthor.'" />';   
                  echo '<h3 class="user-data">' . $user->nickname . '</h3>';
                  echo '<div class="author-description">'.$user->description.'</div>';    
                  echo '<div class="author-description">Web: <a href="'.$user->user_url.'" rel="nofollow">'.$user->user_url.'</a></div>';                
                  echo '</p><h2 class="leading"></h2>';
                }
              }
            }
            ?>          
                      
      </div>  

        </div><!-- end #core .eightcol-->

    <?php get_sidebar(); ?>  

</div><!-- #core -->

<div class="clearfix"></div>
    
<?php get_footer(); ?>