		<span>
            <i class="icon-chevron-sign-left"></i> <?php _e('Precedente','themnific');?>:<br/>
            <?php previous_post_link('%link'); ?>
        </span>
        
        <span>
            <?php _e('Successivo','themnific');?>: <i class="icon-chevron-sign-right"></i><br/>
            <?php next_post_link('%link'); ?>
        </span>