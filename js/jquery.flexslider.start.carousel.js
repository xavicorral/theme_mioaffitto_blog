jQuery(window).load(function() {
/*global jQuery:false */
"use strict";
	
  jQuery('.flexcarousel').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 183,        
	slideshowSpeed: 10000, 
    itemMargin:30,
    minItems: 1,
    maxItems: 5,
	smoothHeight: true
  });
  
	jQuery(document).ready(function () {
		jQuery(".flexcarousel").animate({height:'290'},800);
	});
  
});