<?php
/** A simple text block **/
class AQ_Flexslider_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Flex Slider - for 2/3 Column',
			'size' => 'span8',
		);
		
		//create the block
		parent::__construct('aq_flexslider_block', $block_options);
	}
	
	function form($instance) {
                
	$defaults = array('title' => 'Recent Posts', 'post_type' => 'all', 'categories' => 'all', 'posts' => 4, 'flex_bg_color' => '#eee','flex_text_color' => '#fff',);
	$instance = wp_parse_args((array) $instance, $defaults);
	
			
   	
	extract($instance); ?>		
                
                
        
        <p class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				Title (optional)
				<input id="<?php echo $this->get_field_id('title') ?>" class="input-full" type="text" value="<?php echo $title ?>" name="<?php echo $this->get_field_name('title') ?>">
			</label>
		</p>
        
        <p class="description">
			<label for="<?php echo $this->get_field_id('categories'); ?>">Filter by Category:</label> 
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>all categories</option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
        
        <div class="description half">
			<label for="<?php echo $this->get_field_id('flex_text_color') ?>">
				Pick a link & text color<br/>
				<?php echo aq_field_color_picker('flex_text_color', $block_id, $flex_text_color, $defaults['flex_text_color']) ?>
			</label>
			
		</div>
		
		<p class="description half last">
			<label for="<?php echo $this->get_field_id('posts'); ?>">Number of posts:</label>
			<input class="widefat" style="width: 30px;" id="<?php echo $this->get_field_id('posts'); ?>" name="<?php echo $this->get_field_name('posts'); ?>" value="<?php echo $instance['posts']; ?>" />
		</p>
		<?php
	}
		
		
		function block($instance) {
                extract($instance);
				
		wp_enqueue_script('jquery.flexslider.start.main', get_template_directory_uri() .'/js/jquery.flexslider.start.main.js','','', true);

        $title = $instance['title'];
		$post_type = 'all';
		$categories = $instance['categories'];
		$posts = $instance['posts'];
		
		
		$post_types = get_post_types();
		unset($post_types['page'], $post_types['attachment'], $post_types['revision'], $post_types['nav_menu_item']);
		
		if($post_type == 'all') {
			$post_type_array = $post_types;
		} else {
			$post_type_array = $post_type;
		}
		?>
			<?php if ( $title == "") {} else { ?>
			<h2 class="widget"><a href="<?php echo get_category_link($categories); ?>"><?php echo $title; ?></a></h2>
			<?php } ?>
			
			<?php
			$recent_posts = new WP_Query(array(
				'showposts' => $posts,
				'cat' => $categories,
			));
			?>
            <div class="mainflex flexslider loading">
            <ul class="slides" >
			<?php  while($recent_posts->have_posts()): $recent_posts->the_post();
			$video_input = get_post_meta(get_the_ID(), 'tmnf_video', true);
			?>

			<li>
                    
				<?php if($video_input) {?>
                
                        <?php echo ($video_input); ?>
                        
						<div class="videoinside">
                    
                        <p class="meta" style="color:<?php echo $flex_text_color;?>;">
                            
                            <?php the_time(get_option('date_format')); ?> &bull; 
                            
                            <?php _e('Views','themnific');?>: <?php echo tmnf_post_views(get_the_ID()); ?>
                            
                        </p>
                            
                        <h2><a style="color:<?php echo $flex_text_color;?>;" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 16); ?></a></h2>
                    
                     </div>
                        
                <?php } else {?>
                    
                    <?php if ( has_post_thumbnail()) { ?>
                    
                        <div class="imgwrap">
                    
                            <?php echo tmnf_ratingbar();?>
                        
                             <a href="<?php the_permalink(); ?>" title="<?php the_title();?>" >
                             
                                <?php the_post_thumbnail( 'flex-slider', array('class' => 'tranz')); ?>
                                
                             </a>
                             
                        </div>
                        
                    <?php } ?>
                
                    <div class="flexinside tranz"><span class="bg tranz" style="border:1px solid <?php echo $flex_text_color;?>"></span>
                    
                        <p class="meta" style="color:<?php echo $flex_text_color;?>;border-bottom:1px solid <?php echo $flex_text_color;?>">
                            
                            <?php the_time(get_option('date_format')); ?> &bull; 
                            
                            <?php _e('Views','themnific');?>: <?php echo tmnf_post_views(get_the_ID()); ?>
                            
                        </p>
                            
                        <h2><a style="color:<?php echo $flex_text_color;?>;" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 16); ?></a></h2>
                        
						<p class="teaser" style="color:<?php echo $flex_text_color;?>;"><?php echo themnific_excerpt( get_the_excerpt(), '180'); ?></p>
                    
                     </div>
                
                 <?php }?>
                        
			</li>

			<?php  endwhile; ?>
			</ul>
            </div>
            <?php wp_reset_query(); ?>
			<?php
                
        }
	
}
aq_register_block('AQ_Flexslider_Block');