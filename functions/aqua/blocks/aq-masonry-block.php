<?php
/** A simple text block **/
class AQ_Masonry_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Masonry',
			'size' => 'span12',
		);
		
		//create the block
		parent::__construct('aq_masonry_block', $block_options);
	}
	
	function form($instance) {
                
	$defaults = array('title' => 'Recent Posts', 'post_type' => 'all', 'categories' => 'all', 'flex_bg_color' => '#eee','flex_text_color' => '#000',);
	$instance = wp_parse_args((array) $instance, $defaults);
	
			
   	
	extract($instance); ?>		
                
                
        
        <p class="description half">
			<label for="<?php echo $this->get_field_id('title') ?>">
				Title (optional)
				<input id="<?php echo $this->get_field_id('title') ?>" class="input-full" type="text" value="<?php echo $title ?>" name="<?php echo $this->get_field_name('title') ?>">
			</label>
		</p>
        
        <p class="description half">
			<label for="<?php echo $this->get_field_id('categories'); ?>">Filter by Category:</label> 
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>all categories</option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
		<?php
	}
		
		
		function block($instance) {
                extract($instance);

        $title = $instance['title'];
		$post_type = 'all';
		$categories = $instance['categories'];
		
		
		$post_types = get_post_types();
		unset($post_types['page'], $post_types['attachment'], $post_types['revision'], $post_types['nav_menu_item']);
		
		if($post_type == 'all') {
			$post_type_array = $post_types;
		} else {
			$post_type_array = $post_type;
		}
		?>
			<?php if ( $title == "") {} else { ?>
			<h2 class="widget"><a href="<?php echo get_category_link($categories); ?>"><?php echo $title; ?></a></h2>
			<?php } ?>
            <div class="maso">
            
                <ul class="maso-inn" >
                
                
                    <?php  
                    $recent_posts = new WP_Query(array('showposts' => 1,'cat' => $categories,));
                    while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
        
                        <li class="maso1">
                                
                                <div class="imgwrap">
                            
                                    <?php echo tmnf_ratingbar();?>
                                
                                     <a href="<?php the_permalink(); ?>" title="<?php the_title();?>" >
                                     
                                        <?php the_post_thumbnail( 'maso1', array('class' => 'tranz')); ?>
                                        
                                     </a>
                                     
                                </div>
                                
                                <div class="flexinside tranz"><span class="bg tranz" style="border:1px solid #fff"></span>
                                
                                    <?php tmnf_meta() ?>
                                        
                                    <h3><a style="color:#fff;" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 16); ?></a></h3>
                                    
                                    <p class="teaser"><?php echo themnific_excerpt( get_the_excerpt(), '200'); ?></p>
                                
                                 </div>
                                
                                    
                        </li>
        
                    <?php  endwhile; ?>
                    



                    <?php  
                    $recent_posts = new WP_Query(array('showposts' => 1,'cat' => $categories, 'offset' => 1));
                    while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
        
                        <li class="maso2">


                                <div class="imgwrap">
                            
                                    <?php echo tmnf_ratingbar();?>
                                
                                     <a href="<?php the_permalink(); ?>" title="<?php the_title();?>" >
                                     
                                        <?php the_post_thumbnail( 'maso2', array('class' => 'tranz')); ?>
                                        
                                     </a>
                                     
                                </div>
                                
                                <div class="flexinside tranz"><span class="bg tranz" style="border:1px solid #fff"></span>
                                
                                    <?php tmnf_meta() ?>
                                        
                                    <h4><a style="color:#fff;" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 16); ?></a></h4>
                                
                                 </div>
                                
                                    
                        </li>
        
                    <?php  endwhile; ?>
                    
                    
                
                    <?php  
                    $recent_posts = new WP_Query(array('showposts' => 1,'cat' => $categories, 'offset' => 2));
                    while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
        
                        <li class="maso3">


                                <div class="imgwrap">
                            
                                    <?php echo tmnf_ratingbar();?>
                                
                                     <a href="<?php the_permalink(); ?>" title="<?php the_title();?>" >
                                     
                                        <?php the_post_thumbnail( 'maso3', array('class' => 'tranz')); ?>
                                        
                                     </a>
                                     
                                </div>
                                
                                <div class="flexinside tranz"><span class="bg tranz" style="border:1px solid #fff"></span>
                                
                                    <?php tmnf_meta() ?>
                                        
                                    <h4><a style="color:#fff;" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 16); ?></a></h4>
                                
                                 </div>
                                
                                    
                        </li>
        
                    <?php  endwhile; ?>
                    
                    
                    <?php  
                    $recent_posts = new WP_Query(array('showposts' => 1,'cat' => $categories, 'offset' => 3));
                    while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
        
                        <li class="maso4">


                                <div class="imgwrap">
                            
                                    <?php echo tmnf_ratingbar();?>
                                
                                     <a href="<?php the_permalink(); ?>" title="<?php the_title();?>" >
                                     
                                        <?php the_post_thumbnail( 'maso2', array('class' => 'tranz')); ?>
                                        
                                     </a>
                                     
                                </div>
                                
                                <div class="flexinside tranz"><span class="bg tranz" style="border:1px solid #fff"></span>
                                
                                    <?php tmnf_meta() ?>
                                        
                                    <h4><a style="color:#fff;" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 16); ?></a></h4>
                                
                                 </div>
                                
                                    
                        </li>
        
                    <?php  endwhile; ?>    
                    
                    
                     <?php  
                    $recent_posts = new WP_Query(array('showposts' => 1,'cat' => $categories, 'offset' => 4));
                    while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
        
                        <li class="maso5">


                                <div class="imgwrap">
                            
                                    <?php echo tmnf_ratingbar();?>
                                
                                     <a href="<?php the_permalink(); ?>" title="<?php the_title();?>" >
                                     
                                        <?php the_post_thumbnail( 'maso1', array('class' => 'tranz')); ?>
                                        
                                     </a>
                                     
                                </div>
                                
                                <div class="flexinside tranz"><span class="bg tranz" style="border:1px solid #fff"></span>
                                
                                    <?php tmnf_meta() ?>
                                        
                                    <h4><a style="color:#fff;" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php echo short_title('...', 16); ?></a></h4>
                                
                                 </div>
                                
                                    
                        </li>
        
                    <?php  endwhile; ?>
                    
                                
                
                
                </ul>
            
            </div>
            <?php wp_reset_query(); ?>
			<?php
                
        }
	
}
aq_register_block('AQ_Masonry_Block');