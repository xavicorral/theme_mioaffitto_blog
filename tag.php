<?php get_header(); ?>

<div id="core">

	<div id="content" class="eightcol">

    <h1 class="leading"><?php single_cat_title(); ?></h1>
      <h2 class="leading"><span><?php echo category_description(); ?></span></h2>
      <div class="linea"></div>
            
		<?php if (have_posts()) : ?>
        
     
            <div class="clearfix"></div>

      		<ul class="medpost">
          
				<?php while (have_posts()) : the_post(); ?>
      
						<?php if(has_post_format('gallery'))  {
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('video')){
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('audio')){
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }elseif(has_post_format('image')){
                            echo get_template_part( '/includes/post-types/image' );
                        }elseif(has_post_format('link')){
                            echo get_template_part( '/includes/post-types/link' );
                        }elseif(has_post_format('quote')){
                            echo get_template_part( '/includes/post-types/quote' );
                            } else {
                            echo get_template_part( '/includes/post-types/block-23col-big' );
                        }?>
         		
                <?php endwhile; ?>   <!-- end post -->
                    
     			</ul><!-- end latest posts section-->
      
              <div class="pagination"><?php tmnf_pagination('&laquo;', '&raquo;'); ?></div>
  
              <?php else : ?>
  
                  <h1>Ci dispiace, non ci sono post che contengano i tuoi criteri di ricerca.</h1>
                  <br/>

			<?php endif; ?>

        </div><!-- end #core .eightcol-->

    <?php get_sidebar(); ?>  

</div><!-- #core -->

<div class="clearfix"></div>
    
<?php get_footer(); ?>