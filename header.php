<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title><?php global $page, $paged; wp_title( '|', true, 'right' ); bloginfo( 'name' ); $site_description = get_bloginfo( 'description', 'display' ); echo " | $site_description"; if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s','themnific'), max( $paged, $page ) ); ?></title>

<!-- Set the viewport width to device width for mobile -->

<?php if (get_option('themnific_res_mode_general') <> "true") { ?>
<style type="text/css">
        
    @font-face {
        font-family: 'OpenSans';
        src: url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Light-webfont...');
        src: url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Light-webfont...') format('emb
edded-opentype'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Light-webfont.woff') format('woff'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Light-webfont.ttf') format('truetype'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Light-webfont.svg#OpenSansLight') format('svg');
        font-weight: 300;
        font-style: normal
    }

    @font-face {
        font-family: 'OpenSans';
        src: url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.eot');
        src: url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.woff') format('woff'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.ttf') format('truetype'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Regular-webfont.svg#OpenSansRegular') format('svg');
        font-weight: 400;
        font-style: normal
    }

    @font-face {
        font-family: 'OpenSans';
        src: url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.eot');
        src: url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.woff') format('woff'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.ttf') format('truetype'),url('http://static.beta.enalquiler.com/front-end/fonts/OpenSans-Bold-webfont.svg#OpenSansBold') format('svg');
        font-weight: 600;
        font-style: normal
    }

   
</style>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<?php } ?>
<script type='text/javascript'>
googletag.cmd.push(function() {
        googletag.defineSlot('/42152709/social', [[980, 90],[728, 90]], 'div-gpt-ad-1373450611455-0').addService(googletag.pubads());
        googletag.pubads().collapseEmptyDivs();
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
});
</script>
<?php themnific_head(); ?>
<?php wp_head(); ?>
</head>

     
<body <?php if (get_option('themnific_upper') == 'false' ){ body_class( );} else body_class('upper' ) ?> itemscope itemtype="http://schema.org/WebPage">




<header>
    <div class="container">
        <a title="Specialisti in appartamenti in affitto" href="http://www.mioaffitto.it/" class="logo-ena">
            <img src="http://www.mioaffitto.it/front-end/images/logo-mio.png" alt="Specialisti in appartamenti in affitto">       
        </a>

        <nav class="main-nav">
            <ul class="list-inline hidden-xs">
                <li class="first"><a href="http://www.mioaffitto.it/community.html">Community</a></li>
                <li><a href="http://www.mioaffitto.it/preferiti" data-ena-favourites-menu='' rel="nofollow"><i class="fa fa-star"></i> <span>Preferiti</span></a></li>
                <li class="active"><a href="http://blog.mioaffitto.it">Blog</a></li>
            </ul>
        </nav>

        <div class="access-publish">
            <a class="publish-free btn btn-sm btn-warning" href="http://www.mioaffitto.it/annunciare-immobile-gratis" rel="nofollow">
                <i class="fa fa-plus"></i> Pubblica gratis
            </a>
          
        </div>
    </div>




  <div class="clearfix"></div>
    
        <div id="navigation_wrap">
        
            <a id="triggernav" href="#"><?php _e('MENU','themnific');?></a>
            
            <nav id="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
            
                <?php 
                    get_template_part('/includes/uni-navigation'); 
                    get_template_part('/includes/uni-searchform' );
                ?>
            
            </nav>
    
        </div>
        
    </div>
</header>

<div class="clearfix"></div>

<div class="container <?php if (get_option('themnific_res_mode_general') == 'false' ); else echo 'generalresp_alt'; ?> <?php if (get_option('themnific_upper') == 'false' ); else echo 'upper'; ?>" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">